import { Page } from "playwright";

// via https://stackoverflow.com/a/47092642
async function sleep(msec: number) {
	return new Promise(resolve => setTimeout(resolve, msec));
}

import fs from 'fs';
import { chromium } from 'playwright';

async function deleteTweet(page: Page, url: string) {
	await page.goto(url);

	// wait for the client-side JS to load
	await sleep(3000)

	const tweetDeleted = page.getByText('Sorry, that Tweet has been deleted.')
	if (await tweetDeleted.isVisible({ timeout: 3000 })) {
		console.log(`${url} has already been deleted`)
		return
	}

	if (await page.getByText('Something went wrong. Try reloading.').isVisible({ timeout: 3000 })) {
		console.log(`${url} _may_ have already been deleted`)
		return
	}

	const tweetPath = url.replace('https://twitter.com', '')
	const tweetUrl = page.locator(`css=a[href="${tweetPath}"]`)
	const container = tweetUrl.locator('..').locator('..').locator('..').locator('..').locator('..').locator('..').locator('..')

	const dots = container.getByTestId('caret').first()
	// console.log({ dots })
	await dots.click();


	const deleteButton = page.getByTestId('Dropdown').getByText('Delete');
	await deleteButton.click()

	const confirmDeleteButton = page.getByTestId('confirmationSheetConfirm')
	await confirmDeleteButton.click({ timeout: 3000 })

	// await page.close()

	console.log(`Successfully deleted ${url}`)
}

(async () => {
	if (process.argv.length !== 3) {
		console.log(`Usage: ${process.argv[0]} ${process.argv[1]} /path/to/tweets.json`)
		process.exit(1);
	}

	const browser = await chromium.connectOverCDP('http://localhost:9222');

	// https://www.jvt.me/posts/2023/09/30/playwright-use-existing-session/
	const defaultContext = browser.contexts()[0]
	const page = defaultContext.pages()[0]

	const tweetsJSON = fs.readFileSync(process.argv[2], 'utf-8')
	const tweets = JSON.parse(tweetsJSON)

	const visited = JSON.parse(fs.readFileSync('visited.json', 'utf-8'))

	for (const tweet of tweets) {
		if (tweet.tweet.full_text.startsWith('RT @')) {
			continue
		}

		const url = `https://twitter.com/JamieTanna/status/${tweet.tweet.id}`

		if (url in visited) {
			continue
		}

		console.log(`Starting to process ${url}`)
		await deleteTweet(page, url)

		visited[url] = ""
		fs.writeFileSync('visited.json', JSON.stringify(visited))
	}
})();
