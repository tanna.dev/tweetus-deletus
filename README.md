# tweetus-deletus 🐦🪄💀

A tool to delete all your tweets, based on a Twitter data export, through the browser.

This allows you to drive the deletion through the browser, using [Playwright](https://playwright.dev/), without needing to use any external services or the API.

## Known issues

⚠️ This is janky!  This is the first Playwright code I've written, and I've not tried to make it particularly excellent code.

This only handles some of the edge cases I've seen while running through my own data, and may break at any time. Feel free to raise an Issue / Merge Request, but after I've deleted all my tweets, it's unlikely I'll be using this again!

I've not yet seen any issues around rate limiting from Twitter, but every so often I've had the tab crash, so you may want to keep an eye on it.

There is no parallelisation of deletions - on purpose - to avoid rate limits, and make this a fairly straightforward solution.

## Usage

### Prep

You will need to have downloaded a Twitter export of your data.

Open the export, and find the `tweets.js` and copy it to a new file i.e. `tweets.json`.

Open up `tweets.json`, and make the following change to the first line of the file to make it valid JSON:

```diff
-window.YTD.tweets.part0 = [
+[
```

Save the file and exit.

Next, clone this repository, then run:

```sh
npm run build
```

Finally, create the file `visited.json` (because I was too lazy to create a condition to create it on first use).

```sh
echo '{}' > visited.json
```

### Running

Once you've got your data ready, run:

```sh
# prepare the Chromium instance, that is already logged into your Twitter account. Must be using port 9222, or else you'll need to modify the code.
chromium --remote-debugging-port=9222 &

# then run tweetus-deletus
node dist/index.js path/to/tweets.json
```

This will then start processing and driving the browser to delete the tweets, one-by-one.

## Licensing

Licensed under the Apache-2.0
